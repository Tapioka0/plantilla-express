const express = require("express");
require("dotenv")//.config({ path: "variables.env" });
const app = express();
const PORT = process.env.PORT || 4000;
const { conectarDB } = require("./config/db");
const cors = require("cors");
conectarDB();

app.use(express.json());
app.use(cors());

app.use("/api/status", require("./routes/status"));

app.listen(PORT, () => console.log(`el servidor esta listo en: ${PORT}`));
