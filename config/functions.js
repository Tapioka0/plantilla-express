const db = require("../models/testModel");

const createModel = async (user, password) => {
  const model = new db({
    nombre: user,
    password: password,
  });
  try {
    await model.save();
  } catch (error) {
    console.log(error);
   
  }
};

const getModel = async (user) => {
  const model = db.findOne({
    nombre: user,
  });
  return model;
};

const deleteModel = async (user) => {
  try {
    await db.findOneAndDelete({
      nombre: user,
    });
    console.log("eliminado");
    return true;
  } catch (error) {
    console.log(error);
   
  }
};

const updateModel = async (user, update) => {
  try {
    await db.findOneAndUpdate(
      {
        nombre: user,
      },
      {
        password: update,
      },
      {
        new: true,
      }
    );

    console.log("valor actualizado");
  } catch (error) {
    console.log(error);
   
  }
};


module.exports = { createModel , getModel, deleteModel , updateModel}
