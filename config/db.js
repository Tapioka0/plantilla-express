const mongoose = require("mongoose");
require("dotenv")//.config({ path: "variables.env" });

const conectarDB = async () => {
  try {
    await mongoose.connect(process.env.mongoURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      //  useFindAndModify: false,
    });
    console.log("conectado a la db");
  } catch (error) {
    console.log(error);
    process.exit(1);
  }
};
module.exports = { conectarDB };
