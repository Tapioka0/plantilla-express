const mongoose = require("mongoose");

const test = mongoose.Schema({
  nombre: {
    type: String,
  },

  password: {
    type: String,
  },
});

module.exports = mongoose.model("Usuario", test);
