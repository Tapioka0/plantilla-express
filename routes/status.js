const express = require("express");
const router = express.Router();
const { getStatusFromApi } = require("../controllers/status");
const { check } = require("express-validator");

/*

router.get(
  "/",
  [
    check("nickname", "Nombre es obligatorio").not().isEmpty(),
    check("email", "Agrega un email valido").isEmail(),
    check("password", "debe ser minimo de 6 caracteres").isLength({ min: 6 }),
  ],
  getStatusFromApi
);

*/

router.get("/", getStatusFromApi);

module.exports = router;
